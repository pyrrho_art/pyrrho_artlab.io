---
layout: default
title: "About"
---

Cautious explorer of new media art forms #cleanNFT


*``Destruction of the traditional ... War has been declared on the aesthetic of chaos. An order that has entered fully into consciousness is called for.''* {El Lissitsky, Ideological superstructure, 1929}

<!-- ![](assets/selfie.jpg) -->

**It will hurt someone**
