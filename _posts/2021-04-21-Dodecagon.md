---
layout: post
title:  "Dodecagon"
link: https://www.hicetnunc.xyz/objkt/41058
date: 2021-04-21
img: assets/dod/2_53.jpeg
available:
description: Second item of the new "esoteric clocks" collection. Click on the title above (Dodecagon) to learn how to read the time
object: 41058
editions: 25
categories: [esoteric clocks]
---

## How to read the time
* The background colour indicates whether it is *am* (light) or *pm* (dark)
* The outside vertexes connected with thicker lines indicate the hour
* The thick ray connecting the center to an outside vertex indicate the minute, to 5 minutes approximations.
* Every additional thinner ray indicates one additional minute

## Examples
# 2:51 pm
![](/assets/dod/2_51.jpeg)

# 2:52 pm
![](/assets/dod/2_52.jpeg)

# 2:53 pm
![](/assets/dod/2_53.jpeg)

# 2:54 pm
![](/assets/dod/2_54.jpeg)

# 2:55 pm
![](/assets/dod/2_55.jpeg)

# 8:14 am
![](/assets/dod/8_14.jpeg)

# 3:25 am
![](/assets/dod/3_25.jpeg)

# midnight
![](/assets/dod/midn.jpeg)
