---
layout: post
title:  "Cosmogenesis"
link: https://www.hicetnunc.xyz/objkt/20518
date: 2021-03-27
img: assets/cosmogenesis.gif
available:
description: Generative HTML composition. The NFT version generates a different artwork every time.
object: 14387
editions: 50
categories: [cosmic formations]
---
