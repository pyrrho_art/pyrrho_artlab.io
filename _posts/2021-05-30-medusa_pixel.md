---
layout: post
title:  "Medusa_tool-pixellated"
link: https://www.hicetnunc.xyz/objkt/105479
date: 2021-05-22
img: assets/medusa_pix.gif
available:
description: (Collector edition) This NFT is not an artwork on its own but rather a tool for artists to create highly customised Medusa patterns.
object: 105479
editions: 1
categories: [Medusa]
---
