---
layout: post
title:  "Nostradamus"
link: https://www.hicetnunc.xyz/objkt/54305
date: 2021-04-29
img: assets/nostra.jpg
available:
description: An exploration into ephemeral NFTs in which the artwork has a deadline and the deadline is the artwork itself. Click on the title above (Nostradamus) to learn about this NFT
object: 54305
editions: 25
categories: [Conceptual art]
---
This conceptual artwork is an exploration into ephemeral NFTs. The artwork has a deadline, but the deadline is the artwork itself. If you read this text after May 20 2021 at 20:21, you should not know that, until that date and time, the artwork showed a text "THIS TEXT WILL DISAPPEAR ON THURSDAY MAY 20 2021 AT 20:21" and played an unsettling sound. But will it actually disappear on May 20 at 20:21? What will happen then? You can experience this artwork as it is for a few weeks; then, it will be changed forever. The transition between the two stages of its life will be instantaneous, ephemeral. Will you be there to experience it? And what will happen in different time zones? And will the NFT lose or gain value after its doomsday?

[Link to the NFT](https://hicetnunc.xyz/objkt/54305)
