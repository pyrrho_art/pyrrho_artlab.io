---
layout: post
title:  "Historical amnesia"
link: https://www.hicetnunc.xyz/objkt/60372
date: 2021-05-03
img: assets/maumau.gif
available:
description: This NFT is a reflection on historical events that are purposely forgotten or covered up. The text Mau Mau, which refers to a rebellion against British colonisers in Kenya, will slowly disintegrate and become illegible in the next few months.
object: 60372
editions: 25
categories: [Conceptual art]
---
**Historical amnesia** is a reflection on those horrifying chapters of human history that tend to be forgotten. A few genocides and episodes of human self-destruction are discussed and widely known in the whole world. Others, especially those perpetrated by current dominant Western colonising forces, are dismissed or failed to be acknowledged.

The text *MAU MAU* will slowly disintegrate and become illegible after around six months since this piece was minted. Mau Mau refers to one of these atrocious and semi-forgotten genocides, which was perpetrated in Kenya by British colonisers and that resulted in The Mau Mau rebellion, a violent resistance against colonialism that was brutally suppressed.

This piece's languid pace is also an invitation to slow down and allow space and time to reflect as an effort to contrast the hegemonic fast-food consumeristic art.

![](/assets/amnesia.png)
