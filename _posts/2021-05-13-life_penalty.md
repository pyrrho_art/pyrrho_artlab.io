---
layout: post
title:  "Life penalty"
link: https://www.hicetnunc.xyz/objkt/75662
date: 2021-05-13
img: assets/penalty.gif
available:
description: The viewers' tez address determines whether one is a winner or a loser.
object: 75662
editions: 10
categories: [Medusa]
---
The viewers' tez address determines whether one is a winner (10%) or a loser: winners always score; losers fail forever. This game reflects on the systemic inequalities embedded in our society. Neoliberal views tell that you that there are no systemic inequalities: you are the master of your destiny, and if you fail, you can only blame but yourself; if you succeed, it's because you worked hard. These views fail to account for the inequalities that favour people from specific classes, ethnicities, genders.. The interactive aspect represents the illusion of agency and control over your actions. Despite the text, success in this game is pre-programmed - not under your control. 
