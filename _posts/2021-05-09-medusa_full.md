---
layout: post
title:  "Medusa_tool-color"
link: https://www.hicetnunc.xyz/objkt/70845
date: 2021-05-09
img: assets/m_tool_col.gif
available:
description: This NFT is not an artwork on its own but rather a tool for artists to create highly customised Medusa patterns.
object: 70845
editions: 100
categories: [Medusa]
---
